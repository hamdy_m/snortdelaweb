'''
Created on Feb 8, 2015
'''

__AUTHOR__ = "Mohammed Hamdy"

from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django import forms
from django.forms.models import modelformset_factory
from django.utils.translation import ugettext, ugettext_lazy
from django.core.exceptions import ValidationError
from SnortDeLaWeb.models import Profile, Configuration, Device

class LoginForm(AuthenticationForm):
  """
  Adds a 'remember me' field to django's AuthenticationForm 
  """
  remember_me = forms.BooleanField(required=False)
  
  def remember_user(self, request):
    remember_user = self.cleaned_data["remember_me"]
    if not remember_user:
      request.session.set_expiry(0)
      

class EditProfileForm(forms.ModelForm):
  """
  This should be a bound form to an existing profile object. It doesn't seem like so.
  """
  email = forms.EmailField(required=False)
  old_password = forms.CharField(widget=forms.PasswordInput, required=False)
  new_password1 = forms.CharField(widget=forms.PasswordInput, label=ugettext_lazy("New password"), required=False)
  new_password2 = forms.CharField(widget=forms.PasswordInput, label=ugettext_lazy("Confirm new password"), required=False)
  
  
  class Meta:
    model = Profile
    fields = ["avatar"]
    
  def clean_old_password(self):
    # ensure that the old password, if specified, is correct
    old_password = self.cleaned_data.get("old_password")
    if old_password:
      user = authenticate(self.instance.user.username, old_password)
      if user is None:
        raise ValidationError(ugettext("Invalid old password"), code="invalid")
    return old_password
  
  def clean(self):
    # ensure that new passwords, if specified, match each other
    cleaned_data = super(EditProfileForm, self).clean()
    new_password1 = cleaned_data.get("new_password1")
    new_password2 = cleaned_data.get("new_password2")
    if new_password1 and new_password2:
      if new_password1 != new_password2:
        raise ValidationError(ugettext("New passwords did not match"), code="error")
    return cleaned_data
  
  def update_user(self, user):
    email = self.cleaned_data.get("email")
    if email or (user.email and not email):
      user.email = email
    new_password = self.cleaned_data.get("new_password1")
    if new_password:
      user.set_password(new_password)
    user.save()
    

class UpdateConfigurationForm(forms.ModelForm):
  class Meta:
    model = Configuration
    fields = "__all__"
  

UserNoteFormSet = modelformset_factory(Device, fields=["note", "status"], 
  widgets={"note": forms.Textarea(attrs={"cols":20, "rows":2})})
