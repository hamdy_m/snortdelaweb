'''
Created on Feb 14, 2015
'''

__AUTHOR__ = "Mohammed Hamdy"

from django.db.models.signals import post_save
from django.dispatch import receiver
from SnortDeLaWeb.models import User, Profile

@receiver(post_save, sender=User, dispatch_uid=__AUTHOR__)
def attach_profile_to_user(sender, **kwargs):
  instance = kwargs.get("instance")
  # avoid recursive calls
  try:
    Profile.objects.get(user=instance)
  except Profile.DoesNotExist:
    profile = Profile(user=instance)
    instance.profile = profile
    profile.save()
    instance.save()
