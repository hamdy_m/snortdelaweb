'''
Created on Feb 8, 2015
'''

__AUTHOR__ = "Mohammed Hamdy"


from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib.auth.forms import UserCreationForm
from django.views.generic.edit import FormView, UpdateView
from django.views.generic import TemplateView, View
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as tr
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.http.response import HttpResponseRedirect, HttpResponse
from django.template import Context
from django.template.loader import get_template
from SnortDeLaWeb.models import Profile, Configuration, Device
from SnortDeLaWeb.forms import LoginForm, EditProfileForm,\
  UpdateConfigurationForm, UserNoteFormSet
from weasyprint import HTML
  

class HomeView(TemplateView):
  template_name = "SnortDeLaWeb/site_home.html" 
  

class RegisterUserView(FormView):
  template_name = "SnortDeLaWeb/register_user.html"
  form_class = UserCreationForm
  success_url = reverse_lazy("login_user")
  
  def form_valid(self, form):
    form.save()
    return super(RegisterUserView, self).form_valid(form)
  
  
class LoginView(FormView):
  template_name = "SnortDeLaWeb/login_user.html"
  form_class = LoginForm
  success_url = reverse_lazy("dashboard")
  
  def form_valid(self, form):
    username = self.request.POST["username"]
    password = self.request.POST["password"]
    user = authenticate(username=username, password=password)
    if user is not None:
      login(self.request, user)
      form.remember_user(self.request)
    return super(LoginView, self).form_valid(form)
  

class EditProfileView(UpdateView):
  template_name = "SnortDeLaWeb/edit_profile.html"
  form_class = EditProfileForm
  success_url = reverse_lazy("dashboard")
  
  
  def get_object(self, queryset=None):
    return get_object_or_404(Profile, user=self.request.user)

  def form_valid(self, form):
    form.update_user(self.request.user)
    messages.add_message(self.request, messages.SUCCESS, tr("Profile edited successfully"))
    return super(EditProfileView, self).form_valid(form)
  

class UpdateConfigurationView(UpdateView):
  form_class = UpdateConfigurationForm
  template_name = "SnortDeLaWeb/update_configuration.html"
  success_url = reverse_lazy("dashboard")
  
  def get_object(self, queryset=None):
    return Configuration.objects.all()[0] # there's always one configuration object
  
  def form_valid(self, form):
    messages.add_message(self.request, messages.SUCCESS, tr("Configuration edited successfully"))
    return super(UpdateConfigurationView, self).form_valid(form)
  
  
class DashboardView(TemplateView):
  template_name = "SnortDeLaWeb/dashboard.html"
  
  @method_decorator(login_required)
  def dispatch(self, *args, **kwargs):
    return super(DashboardView, self).dispatch(*args, **kwargs)
    

class DevicesView(TemplateView):
  template_name = "SnortDeLaWeb/devices.html"
  
  def get_context_data(self, **kwargs):
    context = super(DevicesView, self).get_context_data(**kwargs)
    context_devices = []
    user_devices = Device.objects.filter(user=self.request.user)
    device_form_initials = []
    for device in user_devices:
      device_info = {"address":device.address, "os":device.os, "priority":device.priority,
                     "note":"", "malwarename":None, "status":None} # status None means no alerts and thus fined
      device_form_initials.append({"note": device.note})
      # to get the device malwarename and status, get it's latest associated alert
      device_alerts = device.alert_set.all()
      if len(device_alerts) > 0:
        latest_alert = device_alerts.order_by("-timestamp")[0]
        device_info["status"] = latest_alert.status
        device_info["malwarename"] = latest_alert.malware.name
      context_devices.append(device_info)
    # pack the devices and forms together to ease looping in template
    context["devices_forms"] = zip(context_devices, UserNoteFormSet(queryset=user_devices, initial=device_form_initials))
    return context
  
  def post(self, request, *args, **kwargs):
    form_data = request.POST.copy()
    device_count = Device.objects.filter(user=request.user).count()
    form_data.update({'form-TOTAL_FORMS': device_count,'form-INITIAL_FORMS': device_count,
                      'form-MAX_NUM_FORMS': u'',})
    note_formset = UserNoteFormSet(form_data)
    if note_formset.is_valid():
      note_formset.save()
      messages.add_message(request, messages.SUCCESS, tr("Notes edited successfully"))
    return HttpResponseRedirect(reverse("devices"))
      
class ReportsView(TemplateView):
  template_name = "SnortDeLaWeb/reports.html"
  
  
class ReportDownloadView(View):
  
  def get(self, request, *args, **kwargs):
    pdf_template = get_template("SnortDeLaWeb/report_pdf.html")
    context_dict = {}
    context_dict["title"] = tr("Devices Statistics")
    context_dict["compromised"] = request.GET["compromised"]
    context_dict["suspicious"] = request.GET["suspicious"]
    context_dict["remediated"] = request.GET["remediated"]
    context_dict["investigating"] = request.GET["investigating"]
    context_dict["new_malware"] = request.GET["new_malware"]
    pdf_html = HTML(string=pdf_template.render(Context(context_dict)))
    response = HttpResponse(content_type="application/pdf")
    response['Content-Disposition'] = 'attachment; filename="report.pdf"'
    response.write(pdf_html.write_pdf())
    return response
  