'''
Created on Feb 13, 2015
'''

__AUTHOR__ = "Mohammed Hamdy"

from django.db import models
from inspect_model import InspectModel

class InspectModelJSExt(InspectModel):
  """
  Adds mapping between Django field types to Javascript object types.
  Useful to create Google Visualization data models
  see: https://developers.google.com/chart/interactive/docs/reference#DataTable_addColumn
  """
  
  def _get_js_field_type(self, django_field):
    # TODO : support all Django field types
    if isinstance(django_field, (models.CharField, models.TextField, models.IPAddressField)):
      return "string"
    elif isinstance(django_field, (models.IntegerField, models.BigIntegerField, models.FloatField)):
      return "number"
    elif isinstance(django_field, models.DateField):
      return "date"
    elif isinstance(django_field, models.DateTimeField):
      return "datetime"
    elif isinstance(django_field, models.BooleanField):
      return "boolean"
    return None
  
  def get_model_description(self):
    # see : https://developers.google.com/chart/interactive/docs/dev/gviz_api_lib
    model_description = {}
    for field_name in self.fields:
      django_field = getattr(self.model, field_name)
      model_description[field_name] = (self._get_js_field_type(django_field), getattr(django_field, "verbose_name", ''))
    return model_description