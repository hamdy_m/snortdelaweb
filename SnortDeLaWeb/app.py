'''
Created on Feb 14, 2015
'''

__AUTHOR__ = "Mohammed Hamdy"

from django.apps import AppConfig

class SnortDeLaWebAppConfig(AppConfig):
  name = "SnortDeLaWeb"
  verbose_name = "Web Security"
  
  def ready(self):
    from SnortDeLaWeb import signals
    