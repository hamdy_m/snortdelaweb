# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import awesome_avatar.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Alert',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField()),
                ('status', models.CharField(max_length=100, choices=[(b'CM', b'Compromised'), (b'SS', b'Suspicious')])),
                ('protocol', models.CharField(default=b'IP', max_length=100, choices=[(b'IP', b'IP'), (b'ICMP', b'ICMP'), (b'TCP', b'TCP'), (b'UDP', b'UDP')])),
                ('sourcematch', models.BooleanField(default=False)),
                ('destmatch', models.BooleanField(default=False)),
                ('desttag', models.CharField(max_length=100, choices=[(b'DPRV', b'Private'), (b'DGLO', b'Global')])),
                ('sourcetag', models.CharField(max_length=100, choices=[(b'DPRV', b'Private'), (b'DGLO', b'Global')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Configuration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('interface', models.CharField(max_length=255, null=True, blank=True)),
                ('buffer_size', models.IntegerField(null=True, blank=True)),
                ('pcap_size', models.IntegerField(null=True, blank=True)),
                ('pcap_folder', models.CharField(max_length=255, null=True, blank=True)),
                ('url_file', models.CharField(max_length=255, null=True, blank=True)),
                ('feed_folder', models.CharField(max_length=255, null=True, blank=True)),
                ('input_folder', models.CharField(max_length=255, null=True, blank=True)),
                ('merged_file', models.CharField(max_length=255, null=True, blank=True)),
                ('black_file', models.CharField(max_length=255, null=True, blank=True)),
                ('alert_file', models.CharField(max_length=255, null=True, blank=True)),
                ('pcapf', models.CharField(max_length=255, null=True, blank=True)),
                ('ndpf', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Device',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address', models.IPAddressField(unique=True)),
                ('priority', models.IntegerField(null=True, blank=True)),
                ('os', models.CharField(max_length=100, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Flow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ip_source', models.IPAddressField()),
                ('ip_destination', models.IPAddressField()),
                ('port_source', models.IntegerField()),
                ('port_destination', models.IntegerField()),
                ('nb_packets', models.IntegerField()),
                ('application', models.CharField(max_length=255)),
                ('protocol', models.CharField(max_length=100, choices=[(b'IP', b'IP'), (b'ICMP', b'ICMP'), (b'TCP', b'TCP'), (b'UDP', b'UDP')])),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Infection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField()),
                ('sourceip', models.IPAddressField()),
                ('destip', models.IPAddressField()),
                ('sourceport', models.IntegerField()),
                ('destport', models.IntegerField()),
                ('devices', models.ManyToManyField(related_name='infections', to='SnortDeLaWeb.Device')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Malware',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255)),
                ('time_discovered', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Network',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('alias', models.CharField(max_length=255, null=True, blank=True)),
                ('address', models.IPAddressField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('avatar', awesome_avatar.fields.AvatarField(null=True, upload_to=b'C:\\Users\\toxic_000\\workspace\\SnortDeLaWeb\\uploads\\avatars', blank=True)),
                ('user', models.OneToOneField(related_name='profile', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='infection',
            name='malware',
            field=models.ForeignKey(to='SnortDeLaWeb.Malware'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='device',
            name='network',
            field=models.ForeignKey(to='SnortDeLaWeb.Network'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='device',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='alert',
            name='device',
            field=models.ForeignKey(to='SnortDeLaWeb.Device'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='alert',
            name='malware',
            field=models.ForeignKey(to='SnortDeLaWeb.Malware'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='alert',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
