# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SnortDeLaWeb', '0003_auto_20150220_1518'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='alert',
            name='remediated',
        ),
        migrations.RemoveField(
            model_name='alert',
            name='under_investigation',
        ),
        migrations.AddField(
            model_name='device',
            name='status',
            field=models.CharField(default=b'OK', max_length=100, choices=[(b'OK', b'OK'), (b'UI', b'Under investigation'), (b'RM', b'Remediated')]),
            preserve_default=True,
        ),
    ]
