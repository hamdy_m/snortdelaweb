# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SnortDeLaWeb', '0004_auto_20150220_2116'),
    ]

    operations = [
        migrations.AddField(
            model_name='device',
            name='note',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
