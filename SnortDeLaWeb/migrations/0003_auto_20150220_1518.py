# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SnortDeLaWeb', '0002_infection_remediated'),
    ]

    operations = [
        migrations.AddField(
            model_name='alert',
            name='remediated',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='alert',
            name='under_investigation',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
