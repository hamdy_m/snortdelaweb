# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SnortDeLaWeb', '0005_device_note'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alert',
            name='status',
            field=models.CharField(max_length=100, choices=[(b'OK', b'OK'), (b'UI', b'Under investigation'), (b'RM', b'Remediated'), (b'CM', b'compromised'), (b'SP', b'suspicious')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='device',
            name='note',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='device',
            name='status',
            field=models.CharField(default=b'OK', max_length=100, choices=[(b'OK', b'OK'), (b'UI', b'Under investigation'), (b'RM', b'Remediated'), (b'CM', b'compromised'), (b'SP', b'suspicious')]),
            preserve_default=True,
        ),
    ]
