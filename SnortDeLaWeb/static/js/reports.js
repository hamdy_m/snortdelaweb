$(document).ready(function () {
  var selected_start, selected_end;
  Dajaxice.SnortDeLaWeb.get_report_date_range(function (date_ranges) {
	if (! date_ranges.has_data ) {
	  $("#report-container").prepend($("<h2>").attr("id", "warn-no-data").text("No Data Found"));
	} else {
	  selected_start = new Date(date_ranges.earliest);
	  selected_end = new Date(date_ranges.latest);
	  $("#date-start-picker").datetimepicker({
		minDate:selected_start,
		maxDate:selected_end,
		defaultDate:selected_start
	  }).on("changeDate", updateReport1);
	  $("#date-end-picker").datetimepicker({
	     minDate:selected_start,
	     maxDate:selected_end,
	     showTodayButton:true,
	     defaultDate:selected_start
	  }).on("changeDate", updateReport2);
	  requestReport();
	}
  })
  function updateReport1(event) {
	// handles start date selection
	selected_start = event.date;
	$("#date-end-picker").options({minDate:event.date});
	if (selected_end == undefined) {
	  return;
	}
	requestReport();
  }
  function updateReport2(event) {
	// handles end date selection
	selected_end = event.date;
	if (selected_start == undefined) {
	  return;
	}
	requestReport();
  }
  function requestReport() {
	Dajaxice.SnortDeLaWeb.get_report_data(function (report_data) {
	  $("#suspicious").text(report_data.suspicious);
	  $("#compromised").text(report_data.compromised);
	  $("#remediated").text(report_data.remediated);
	  $("#investigating").text(report_data.investigating);
	  $("#new-malware").text(report_data.new_malware);
	}, {start_date:selected_start, end_date:selected_end});
  }
  
  // attach handler to export pdf button
  $("#export-pdf").click(function(event) {
	event.preventDefault();
	// collect url params from displayed stats. these will be passed to the next view
	var url_params = {compromised:$("#compromised").text(), suspicious:$("#suspicious").text(),
			remediated:$("#remediated").text(), investigating:$("#investigating").text(),
			new_malware:$("#new-malware").text()};
	var url = "/reports/download/?" + $.param(url_params);
	window.open(url, "_blank");
  })
});