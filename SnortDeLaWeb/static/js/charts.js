google.load('visualization', '1.0', {'packages':['corechart']});

google.setOnLoadCallback(drawCharts);

function drawCharts() {
  // this draws the alerts and flow charts
  Dajaxice.SnortDeLaWeb.get_user_json_alerts(function (alert_data) {
    var data_table = new google.visualization.DataTable(alert_data);
    var alert_chart = new google.visualization.ScatterChart(document.getElementById("alerts"));
    var current_year = new Date().getFullYear();
    var current_month = new Date().getMonth() + 1;
    alert_chart.draw(data_table, {"title":"Alerts Per Day", hAxis:{title:"Alert Count"}, 
    	vAxis:{title:"Date", minValue:new Date(current_year, current_month), 
    		maxValue:new Date(current_year, current_month + 1)}});
  });
  Dajaxice.SnortDeLaWeb.get_user_json_flows(function (flow_data) {
	  var data_table = new google.visualization.DataTable(flow_data);
	  var flow_chart = new google.visualization.PieChart(document.getElementById("flows"));
	  flow_chart.draw(data_table, {"title":"Attacks Per Application"})
  });
  Dajaxice.SnortDeLaWeb.get_compromised_suspicious(function (cs_data) {
	 var data_table = new google.visualization.DataTable(cs_data);
	 var column_chart = new google.visualization.ColumnChart(document.getElementById("comp-susp"));
	 column_chart.draw(data_table, {title:"Infection Statistics", hAxis:{title:"Machine Status"}, vAxis:{title:"Count"}});
  });
}