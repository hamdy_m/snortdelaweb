'''
Created on Feb 14, 2015
'''

__AUTHOR__ = "Mohammed Hamdy"

from django.test import TestCase, Client
from django.core.urlresolvers import reverse


class ViewTestCase(TestCase):
  
  fixtures = ["initial_data.json"]
  test_url_prefix = "http://testserver"
  
  def test_signup(self):
    signup_resp = self.client.post(reverse("register_user"), {"username":"testuser", "password1":"123456", "password2":"123456"})
    destination = signup_resp.url
    self.assertEqual(destination, self.test_url_prefix + reverse("login_user"))
  
  def test_login(self):
    login_response = self.client.post(reverse("login_user"), {"username":"blueblood", "password":"123456"}, 
                                 follow=True)
    self.assertEqual(login_response.status_code, 200, "Invalid login response code {}"
                    .format(login_response.status_code))
    redirected_to_url = login_response.redirect_chain[-1][0]
    self.assertEqual(redirected_to_url, self.test_url_prefix + reverse("dashboard"))
    
