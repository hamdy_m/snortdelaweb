'''
Created on Feb 13, 2015
'''

__AUTHOR__ = "Mohammed Hamdy"

import json
from django.db.models import Count
from dajaxice.decorators import dajaxice_register
import itertools,  datetime
from SnortDeLaWeb.gviz_api import gviz_api
from SnortDeLaWeb.models import Alert, Flow, Device, Malware


@dajaxice_register(method="GET")
def get_user_json_alerts(request):
  # group alerts by day
  user_alerts = Alert.objects.filter(user=request.user).values("timestamp").order_by("timestamp").all()
  date_comparison_format = "%Y/%m/%d"
  alerts_per_day = itertools.groupby(user_alerts, lambda alert : alert["timestamp"].strftime(date_comparison_format))
  # since keys in alerts_per_day are now formatted dates, we need to convert them back to datetime
  row_dicts = [{"timestamp":datetime.datetime.strptime(key, date_comparison_format), "count":len(list(value))} 
               for key, value in alerts_per_day]
  data_table = gviz_api.DataTable({"timestamp":("datetime", "Day"), "count":("number", "Alert Count")})
  data_table.LoadData(row_dicts)
  return data_table.ToJSon()

@dajaxice_register(method="GET")
def get_user_json_flows(request):
  # group flows by application
  user_flows = Flow.objects.filter(user=request.user).values("application").annotate(per_app=Count("application"))
  row_dicts = [{"app":o["application"], "count":o["per_app"]} for o in user_flows]
  data_table = gviz_api.DataTable({"app":("string", "Application Name"), "count":("number", "Flow Frequency")})
  data_table.LoadData(row_dicts)
  return data_table.ToJSon()

@dajaxice_register(method="GET")
def get_compromised_suspicious(request):
  # simple bar chart for compromised vs. suspicious machines
  user_alerts = Alert.objects.filter(user=request.user)
  compromised_count = user_alerts.filter(status=Alert.STATUS_COMPROMISED).count()
  suspicious_count = user_alerts.filter(status=Alert.STATUS_SUPSPICIOUS).count()
  row_dicts = [{"status":"Compromised", "count":compromised_count}, {"status":"Suspicious", "count":suspicious_count}]
  data_table = gviz_api.DataTable({"status":("string", "Status"), "count":("number", "Machine Count")})
  data_table.LoadData(row_dicts)
  return data_table.ToJSon(columns_order=["status", "count"])

@dajaxice_register(method="GET")
def get_report_date_range(request):
  # returns the earliest and latest dates between which we have alert data
  user_alerts = Alert.objects.filter(user=request.user).order_by("timestamp").all()
  if len(user_alerts) < 2:
    return json.dumps({"has_data":False})
  earliest_date, latest_date = user_alerts[0].timestamp, user_alerts[user_alerts.count() - 1].timestamp
  return json.dumps({"has_data":True, "earliest":earliest_date.isoformat(), "latest":latest_date.isoformat()})

@dajaxice_register(method="GET")
def get_report_data(request, start_date, end_date):
  user_devices = Device.objects.filter(user=request.user)
  compromised_count = user_devices.filter(status=Device.STATUS_COMPROMISED).count()
  suspicious_count = user_devices.filter(status=Device.STATUS_SUSPICIOUS).count()
  remediated_count = user_devices.filter(status=Device.STATUS_REMEDIATED).count()
  under_inv_count = user_devices.filter(status=Device.STATUS_UNDER_INVESTIGATION).count()
  new_malware_count = Malware.objects.count()
  return json.dumps({"compromised":compromised_count, "suspicious":suspicious_count,
                     "remediated":remediated_count, "investigating":under_inv_count,
                     "new_malware":new_malware_count})
  