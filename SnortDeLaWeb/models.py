'''
Created on Feb 9, 2015
'''

__AUTHOR__ = "Mohammed Hamdy"

from os import path
import json
from django.contrib.auth.models import User
from django.db import models
from django.conf import settings
from awesome_avatar.fields import AvatarField
from inspect_model import InspectModel

class Profile(models.Model):
  user = models.OneToOneField(User, related_name="profile")
  avatar = AvatarField(upload_to=path.join(settings.MEDIA_ROOT, "avatars"), blank=True, null=True)
  

class Configuration(models.Model):
  """
  Represents the conf.ini in the database
  """
  interface = models.CharField(max_length=255, null=True, blank=True)
  buffer_size = models.IntegerField(null=True, blank=True)
  pcap_size = models.IntegerField(null=True, blank=True)
  pcap_folder = models.CharField(max_length=255, null=True, blank=True)
  url_file = models.CharField(max_length=255, null=True, blank=True)
  feed_folder = models.CharField(max_length=255, null=True, blank=True)
  input_folder = models.CharField(max_length=255, null=True, blank=True)
  merged_file = models.CharField(max_length=255, null=True, blank=True)
  black_file = models.CharField(max_length=255, null=True, blank=True)
  alert_file = models.CharField(max_length=255, null=True, blank=True)
  pcapf = models.CharField(max_length=255, null=True, blank=True)
  ndpf = models.CharField(max_length=255, null=True, blank=True)
  
  def __repr__(self):
    conf_fields = InspectModel(Configuration).fields
    conf_values = map(lambda field_name: getattr(self, field_name), conf_fields)
    result = ''
    for field_name, field_value in zip(conf_fields, conf_values):
      result += ':'.join([field_name, str(field_value)]) + ' '
    return "<Configuration {}>".format(result.rstrip())
  
  
class Network(models.Model):
  alias = models.CharField(max_length=255, blank=True, null=True)
  address = models.IPAddressField(null=True, blank=True)
  
  def __repr__(self):
    return "<Network {}>".format(self.address)
  
  
class DeviceManager(models.Manager):
  
  def from_json(self, filename, user):
    with open(filename, "rt") as record_file:
      for record_line in record_file:
        record_dict = json.loads(record_line)
        # use get_or_create() because devices can be created from Alert model
        self.get_or_create(address=record_dict["device"], defaults={"os":record_dict["os"],
           "priority":record_dict["priority"], "user":user})
        
  
class Device(models.Model):
  STATUS_REMEDIATED = "RM"
  STATUS_UNDER_INVESTIGATION = "UI"
  STATUS_OK = "OK"
  STATUS_COMPROMISED = "CM"
  STATUS_SUSPICIOUS = "SP"
  STATUS_CHOICES = [(STATUS_OK, "OK"), (STATUS_UNDER_INVESTIGATION, "Under investigation"),
                    (STATUS_REMEDIATED, "Remediated"), (STATUS_COMPROMISED, "compromised"),
                    (STATUS_SUSPICIOUS, "suspicious")]
  STATUS_VALUE_MAP = {"compromised":STATUS_COMPROMISED, "suspicious":STATUS_SUSPICIOUS}
  address = models.IPAddressField(unique=True)
  priority = models.IntegerField(null=True, blank=True)
  os = models.CharField(max_length=100, null=True, blank=True)
  # TODO : rename status to case
  status = models.CharField(max_length=100, choices=STATUS_CHOICES, default=STATUS_OK)
  note = models.TextField(null=True, blank=True)
  network = models.ForeignKey(Network)
  user = models.ForeignKey(User)
  objects = DeviceManager()
  
  def __init__(self, *args, **kwargs):
    # add a network to device, if not specified
    if kwargs.get("network") is None:
      network = Network()
      network.save()
      kwargs["network"] = network
    super(Device, self).__init__(*args, **kwargs)
      
  
  def __repr__(self):
    return "<Device {} OS:{}>".format(self.address, self.os)
  
  
class Malware(models.Model):
  name = models.CharField(max_length=255, unique=True)
  time_discovered = models.DateTimeField(auto_now=True)
  
  def __repr__(self):
    return "<Malware {}>".format(self.name)


class Infection(models.Model):
  # this keeps a history of infections for each device
  malware = models.ForeignKey(Malware)
  timestamp = models.DateTimeField()
  sourceip = models.IPAddressField()
  destip = models.IPAddressField()
  sourceport = models.IntegerField()
  destport = models.IntegerField()
  remediated = models.BooleanField(default=False)
  devices = models.ManyToManyField(Device, related_name="infections")
  
  def __repr__(self):
    return "<Infection {}>".format(self.malware.name)


class AlertManager(models.Manager):
  """
  Adds filling the database from JSON files
  """
  
  def from_json(self, filename, user):
    with open(filename) as json_input:
      for record in json_input:
        record_dict = json.loads(record)
        record_dict["desttag"] = Alert.DESTTAG_VALUE_MAP[record_dict["desttag"]]
        record_dict["status"] = Alert.STATUS_VALUE_MAP[record_dict["status"]]
        device, _ = Device.objects.get_or_create(address=record_dict.pop("device"), 
                      defaults={"user":user, "status":record_dict["status"]})
        malware, _ = Malware.objects.get_or_create(name=record_dict.pop("malwarename")) 
        infection = Infection(malware=malware, timestamp=record_dict["timestamp"], 
                      sourceip=record_dict.pop("sourceip"), destip=record_dict.pop("destip"),
                      sourceport=record_dict.pop("sourceport"), destport=record_dict.pop("destport"))
        infection.save()
        infection.devices.add(device)
        infection.save()
        alert = Alert(device=device, malware=malware, user=user, **record_dict)
        alert.save()
  

class Alert(models.Model):
  STATUS_COMPROMISED = Device.STATUS_COMPROMISED
  STATUS_SUPSPICIOUS = Device.STATUS_SUSPICIOUS
  STATUS_CHOICES = Device.STATUS_CHOICES
  STATUS_VALUE_MAP = Device.STATUS_VALUE_MAP
  PROTOCOL_TCP = "TCP"
  PROTOCOL_UDP = "UDP"
  PROTOCOL_ICMP = "ICMP"
  PROTOCOL_IP = "IP"
  PROTOCOL_CHOICES = ((PROTOCOL_IP, "IP"),
                      (PROTOCOL_ICMP, "ICMP"),
                      (PROTOCOL_TCP, "TCP"),
                      (PROTOCOL_UDP, "UDP"))
  DESTTAG_PRIVATE = "DPRV"
  DESTTAG_GLOBAL = "DGLO"
  DESTTAG_CHOICES = ((DESTTAG_PRIVATE, "Private"),
                     (DESTTAG_GLOBAL, "Global"))
  DESTTAG_VALUE_MAP = {"private":DESTTAG_PRIVATE, "global":DESTTAG_GLOBAL}
  user = models.ForeignKey(User)
  timestamp = models.DateTimeField()
  status = models.CharField(max_length=100, null=False, choices=STATUS_CHOICES)
  protocol = models.CharField(max_length=100, null=False, choices=PROTOCOL_CHOICES, default=PROTOCOL_IP)
  sourcematch = models.BooleanField(default=False, null=False)
  destmatch = models.BooleanField(default=False, null=False)
  desttag = models.CharField(max_length=100, choices=DESTTAG_CHOICES)
  sourcetag = models.CharField(max_length=100, choices=DESTTAG_CHOICES)
  device = models.ForeignKey(Device)
  malware = models.ForeignKey(Malware)
  objects = AlertManager()
  
  def __repr__(self):
    return "<Alert for {} at {}>".format(self.user.username, self.timestamp)
  

class FlowManager(models.Manager):
  
  def from_json(self, filename, user):
    with open(filename) as json_file:
      for flow_record in json_file:
        record_dict = json.loads(flow_record)
        record_dict.pop("fow-id", None)
        Flow(user=user, **record_dict).save()


class Flow(models.Model):
  user = models.ForeignKey(User)
  ip_source = models.IPAddressField()
  ip_destination = models.IPAddressField()
  port_source = models.IntegerField()
  port_destination = models.IntegerField()
  nb_packets = models.IntegerField()
  application = models.CharField(max_length=255)
  protocol = models.CharField(max_length=100, choices=Alert.PROTOCOL_CHOICES)
  
  objects = FlowManager()
  
  def __repr__(self):
    return "<Flow from {} to {} by {}>".format(self.ip_source, self.ip_destination, self.application)