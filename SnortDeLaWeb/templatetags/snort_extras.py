'''
Created on Feb 15, 2015
'''

__AUTHOR__ = "Mohammed Hamdy"

from django import template
from django.core.urlresolvers import resolve
from django.utils.translation import ugettext as tr
from SnortDeLaWeb.models import Alert

register = template.Library()

@register.filter(name="activate_if_active", is_safe=True)
def activate_if_active(request, urlname):
  if resolve(request.get_full_path()).url_name == urlname:
    return "active"
  return ''

@register.filter(name="status_class", is_safe=True)
def status_class(status):
  if status is None:
    return "success"
  elif status == "compromised":
    return "danger"
  elif status == "suspicious":
    return "warning"
  
@register.filter(name="malwarename_class", is_safe=True)
def malwarename_class(malwarename):
  if malwarename is None:
    return "success"
  return "danger"

@register.filter(name="case_class", is_safe=True)
def case_class(case):
  if case == 0 or case == 1:
    return "success"
  elif case == 2:
    return "info"
  
@register.filter(name="case_string", is_safe=True)
def case_string(case):
  if case == 0:
    return tr("Checked")
  elif case == 1:
    return tr("Remediated")
  elif case == 2:
    return tr("Under investigation")
  
@register.filter(name="status_string", is_safe=True)
def status_string(status):
  if status is None:
    return tr("OK")
  elif status == Alert.STATUS_COMPROMISED:
    return tr("Compromised")
  elif status == Alert.STATUS_SUPSPICIOUS:
    return tr("Suspicious")