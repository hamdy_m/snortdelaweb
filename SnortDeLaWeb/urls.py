from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import logout as django_logout_view
from django.conf import settings
from django.core.urlresolvers import reverse_lazy
from dajaxice.core import dajaxice_autodiscover, dajaxice_config
from SnortDeLaWeb import views

dajaxice_autodiscover()

user_patterns = patterns('',
  url("^login/", views.LoginView.as_view(), name="login_user"),
  url(r"^logout/$", lambda request : django_logout_view(request, next_page=reverse_lazy("login_user")),
    name="logout_user"),
  url(r"^new/$", views.RegisterUserView.as_view(), name="register_user"),
  url(r"^editprofile/$", views.EditProfileView.as_view(), name="edit_profile")
)

urlpatterns = patterns('',
  url(r"^$", views.HomeView.as_view(), name="site_home"),
  url(r"^dashboard/$", views.DashboardView.as_view(), name="dashboard"),
  url(r"^edit_configuration/$", views.UpdateConfigurationView.as_view(), name="edit_configuration"),
  url(r'^admin/', include(admin.site.urls)),
  url(r"^users/", include(user_patterns)),
  url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),
  url(r"^devices/", views.DevicesView.as_view(), name="devices"),
  url(r"^reports/$", views.ReportsView.as_view(), name="reports"),
  url(r"^reports/download/", views.ReportDownloadView.as_view(), name="download_report")
)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += user_patterns